function one() {
  console.log("Function 1 is executing...");

  two();

  function two() {
    console.log("Function 2 is executing...");

    three();

    function three() {
      console.log("Function 3 is executing...");
    }
  }
}

console.log("Code in the GLOBAL SCOPE is executing...");

one();
