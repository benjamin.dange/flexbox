function one(callback) {
  console.log("Code in the GLOBAL SCOPE is executing...");
  console.log("Function 1 is executing...");

  callback();
}

function two() {
  console.log("Function 2 is executing...");
}

function three() {
  console.log("Function 3 is executing...");
}

one(two);
